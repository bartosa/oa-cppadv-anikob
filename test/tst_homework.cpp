//
// Created by aniko on 2020. 02. 07..
//
#include <src/util.h>
#include <src/numberhandler.h>

#include <3rdparty/catch.hpp>

#include <list>

using namespace std;

auto iseven = [](int i) { return i % 2 == 0; };

TEST_CASE("Util") {

    SECTION("Random") {
        SECTION("1-10") {
            REQUIRE(Util::random<int>(1, 10) <= 10);
            REQUIRE(Util::random<int>(1, 10) >= 1);
        };
        SECTION("double") {
            REQUIRE(Util::random<double>(1.0, 10.0) <= 10.0);
            REQUIRE(Util::random<double>(1.0, 10.0) >= 1.0);
        };
        SECTION("1-1") {
            REQUIRE(Util::random<int>(1, 1) == 1);
        };
        SECTION("-2 2") {
            REQUIRE(Util::random<int>(-2, 2) <= 2);
            REQUIRE(Util::random<int>(-2, 2) >= -2);
        };
    }

    SECTION("Prime") {
        SECTION("-3") {
            REQUIRE(Util::isPrime(-3) == false);
        };
        SECTION("0") {
            REQUIRE(Util::isPrime(0) == false);
        };
        SECTION("1") {
            REQUIRE(Util::isPrime(1) == false);
        };
        SECTION("2") {
            REQUIRE(Util::isPrime(2) == true);
        };
        SECTION("3") {
            REQUIRE(Util::isPrime(3) == true);
        };
        SECTION("11") {
            REQUIRE(Util::isPrime(11) == true);
        };
        SECTION("15") {
            REQUIRE(Util::isPrime(15) == false);
        };
        SECTION("2.5") {
            REQUIRE(Util::isPrime(2.5) == false);
        };
    }

    SECTION("Formatlist ") {
//   Stringify and join container elements.
        SECTION("vector") {
            std::vector<unsigned int> nums{1, 2, 3, 4};
            REQUIRE(Util::formatList(nums) == "1234");
        };
        SECTION("list") {
            std::list<int> nums{1, 2, 3, 4};
            REQUIRE(Util::formatList(nums) == "1234");
        };
        SECTION("double") {
            std::list<double> nums{1.0, 2.4, 3, 4};
            REQUIRE(Util::formatList(nums) == "12.434");
        };
        SECTION("string") {
            std::vector<string> nums{"a", "b", "c"};
            REQUIRE(Util::formatList(nums) == "abc");
        };
        SECTION("char") {
            std::list<char> nums{'a', 'b', 'c'};
            REQUIRE(Util::formatList(nums) == "abc");
        };
    }
}

TEST_CASE("Numberhandler") {

    SECTION("Fill ") {
        NumberHandler<int, std::vector<int>> numbers;
        int i = 0;
        numbers.fill(8, [&i]() { return i++; });
        REQUIRE(numbers.list() == std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7});
    }

    SECTION("Select") {
        //    Return all items from data container matching filter.
        NumberHandler<int, std::vector<int>> numbers;
        int i = 0;
        numbers.fill(10, [&i]() { return i++; });
        std::vector<int> match = numbers.select(iseven);
        // for(auto l : match) cout<<"l: "<< l <<"\n";
        REQUIRE(match == std::vector<int>{0, 2, 4, 6, 8});
    }

    SECTION("List ") {
        //    Get current content of data container.
        SECTION("vecor 1") {
            NumberHandler<int, std::vector<int>> numbers;
            numbers.fill(3, []() { return 1; });
            REQUIRE(numbers.list() == std::vector<int>{1, 1, 1});
        };
        SECTION("list 1") {
            NumberHandler<int, std::list<int>> numbers;
            numbers.fill(3, []() { return 1; });
            REQUIRE(numbers.list() == std::list<int>{1, 1, 1});
        };
        SECTION("vecor 123") {
            NumberHandler<int, std::vector<int>> numbers;
            int i = 0;
            numbers.fill(3, [&i]() { return i++; });
            REQUIRE(numbers.list() == std::vector<int>{0, 1, 2});
        };
    }

}

