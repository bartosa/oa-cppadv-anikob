//
// Created by aniko on 2020. 02. 07..
//

#ifndef OA_CPPADV_ANIKOB_NUMBERHANDLER_H
#define OA_CPPADV_ANIKOB_NUMBERHANDLER_H

#include <iostream>
#include <functional>
#include <algorithm>
//#include <vector>


template<class T, class CONTAINER>
class NumberHandler {
public:
    void fill(int count,
              std::function<T()> generator);    //OK //TESTED  Add count items to data container, where each item is an output of generator function.

    CONTAINER
    select(std::function<bool(T)> filter); //OK // TESTED    Return all items from data container matching filter.

    const CONTAINER &list() const; //Ok TESTED    Get current content of data container.

private:
    CONTAINER conti;
};

template<class T, class CONTAINER>
void NumberHandler<T, CONTAINER>::fill(int count, std::function<T()> generator) {
//  Add count items to data container, where each item is an output of generator function.
    std::generate_n(std::back_inserter(conti), count, generator);
}

template<class T, class CONTAINER>
CONTAINER NumberHandler<T, CONTAINER>::select(std::function<bool(T)> filter) {
    //    Return all items from data container matching filter.
    CONTAINER match(conti.size());
    auto it = std::copy_if(conti.begin(), conti.end(), match.begin(), filter);
    match.resize(std::distance(match.begin(), it));
    return match;
}

template<class T, class CONTAINER>
const CONTAINER &NumberHandler<T, CONTAINER>::list() const {
    return conti;
}


#endif //OA_CPPADV_ANIKOB_NUMBERHANDLER_H
