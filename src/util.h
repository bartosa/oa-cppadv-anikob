//
// Created by aniko on 2020. 02. 07..
//

#ifndef OA_CPPADV_ANIKOB_UTIL_H
#define OA_CPPADV_ANIKOB_UTIL_H

#include <iostream>
#include <functional>
#include <random>
#include <typeinfo>
//#include <format>
#include <sstream>


class Util {
public:

    template<typename T>
    static T random(T min, T max);// OK TESTED Generate a random number in [min, max] range.

    template<typename T>
    static bool isPrime(T number); // Checks if number is prime.

    //Brand new...mi?!
    static std::string formatList(auto container); // OK TESTED Stringify and join container elements.
};

template<typename T>
T Util::random(T min, T max) {
    // Generate a random number in [min, max] range.
    std::random_device generator;
    std::uniform_int_distribution<int> distribution(min, max);
    return distribution(generator);
}

template<typename T>
bool Util::isPrime(T number) {
    // Checks if number is prime.
    if (typeid(number) != typeid(int) || number <= 1) {
        return false;
    }

    for (int i = 2; i <= number / 2; ++i) {
        if ((int) number % i == 0) {
            return false;
        }
    }
    return true;
}


std::string Util::formatList(auto container) {
    // Stringify and join container elements.
    std::ostringstream ss;
    for (auto i: container) {
        ss << i << " ";
    }
    ss << std::endl;
    return ss.str(); //to_string is only for number
}

#endif //OA_CPPADV_ANIKOB_UTIL_H
